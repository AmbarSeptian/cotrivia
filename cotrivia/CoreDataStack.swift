//
//  CoreDataStack.swift
//  cotrivia
//
//  Created by MacBook Pro on 9/2/16.
//  Copyright © 2016 PT. Code Development Indonesia. All rights reserved.
//

import Foundation
import CoreData

typealias initCallbackBlock = (() -> Void)

class CoreDataStack: NSObject {
    
    var callback: initCallbackBlock!
    
    var managedObjectContext: NSManagedObjectContext?
    
    var privateContext: NSManagedObjectContext?
    
    convenience init(callback: initCallbackBlock) {
        self.init()
        self.callback = callback
        self.initializeCoreData()
    }
    
    
    func initializeCoreData() {
        
        guard self.managedObjectContext == nil else { return }
        
        let productName = NSBundle.mainBundle().infoDictionary!["CFBundleName"] as! String
        
        guard let modelURL = NSBundle.mainBundle().URLForResource(productName, withExtension: "momd") else {
            fatalError("Error loading model from bundle")
        }
        
        guard let mom = NSManagedObjectModel(contentsOfURL: modelURL) else {
            fatalError("Error initialize mom from \(modelURL)")
        }
        
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: mom)
        
        self.managedObjectContext =  NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        self.privateContext = NSManagedObjectContext(concurrencyType: .PrivateQueueConcurrencyType)
        
        self.privateContext!.persistentStoreCoordinator = coordinator
        self.managedObjectContext!.parentContext = self.privateContext
        
        
        dispatch_async(kGlobalQueue.Background) {
            
            let psc = self.privateContext?.persistentStoreCoordinator
            let options = [NSMigratePersistentStoresAutomaticallyOption : true,
                           NSInferMappingModelAutomaticallyOption : true,
                           NSSQLitePragmasOption : ["journal_mode" : "DELETE"]]
            
            let fileManager = NSFileManager.defaultManager()
            
            guard let documentsURL = fileManager.URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).last else {
                return
            }
            
            let storeURL = documentsURL.URLByAppendingPathComponent("\(productName).sqlite")
            
            
            do {
                try psc?.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: storeURL, options: options)
            } catch {
                fatalError("Error Migrating \(error)")
            }
            
            
            dispatch_async(kGlobalQueue.Main) {
                self.callback
            }
            
            
        }
    }
    
    
    func save(completion: CompletionSuccess?) {
        
        guard self.privateContext != nil && self.managedObjectContext != nil else {
            return
        }
        
        guard self.privateContext!.hasChanges || self.managedObjectContext!.hasChanges else {
            return
        }
        
        self.managedObjectContext?.performBlockAndWait({
            
            do {
                try self.managedObjectContext?.save()
            } catch {
                if let wCompletion = completion {
                    wCompletion(success: false)
                    fatalError("Error save managedObjectContext \(error)")
                }
                
            }
            
            
            self.privateContext?.performBlock({
                do {
                    try self.privateContext?.save()
                        if let wCompletion = completion {
                            wCompletion(success: true)
                        }
                        
                    } catch {
                        if let wCompletion = completion {
                            wCompletion(success: false)
                            fatalError("Error save privateContext \(error)")
                        }
                        
                    }
                })
                
            })
        }
}

