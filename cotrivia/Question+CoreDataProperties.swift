//
//  Question+CoreDataProperties.swift
//  cotrivia
//
//  Created by MacBook Pro on 9/2/16.
//  Copyright © 2016 PT. Code Development Indonesia. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Question {

    @NSManaged var id: String?
    @NSManaged var type: NSNumber?
    @NSManaged var questionString: String?
    @NSManaged var trueAnswer: NSNumber?
    @NSManaged var subject: Subject?
    @NSManaged var answers: NSSet?

}
