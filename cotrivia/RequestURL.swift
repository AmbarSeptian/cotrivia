//
//  RequestURL.swift
//  cotrivia
//
//  Created by MacBook Pro on 9/2/16.
//  Copyright © 2016 PT. Code Development Indonesia. All rights reserved.
//
//
//import Foundation
//
//
//protocol RequestURLProtocol { }
//
//extension RequestURLProtocol {
//    
//    
//    
//    //    2. Subjects
//    //
//    //    - Select All and ByTimestamp (GET)  : http://128.199.68.208/cotrivia/api/index.php/subjects/timestamp/{timestamp}
//    //
//    //    jika select all, timestamp nya diisi 0.
//    //
//    //    - SelectByID (GET)	   				: http://128.199.68.208/cotrivia/api/index.php/subjects/view/{id}
//    //
//    //    - Subject detail (GET) 		: http://128.199.68.208/cotrivia/api/index.php/subjects/detail/{id_subject}
//    //
//    //    3. Users
//    //
//    //    - Login (POST) 				: http://128.199.68.208/cotrivia/api/index.php/users/login
//    //
//    //    POST :  {
//    //    "id"			:"{facebook id}",
//    //    "name"			:"{facebook name}",
//    //    "profile_pic"	:"{url profile picture}"
//    //				}
//    //
//    //    4. Scores
//    //
//    //    - SelectByUserID (POST)		: http://128.199.68.208/cotrivia/api/index.php/scores/view
//    //
//    //    POST : { "id":"{facebookid}"}
//    //
//    //    - SaveScore	(POST) 			: http://128.199.68.208/cotrivia/api/index.php/scores
//    //    
//    //    POST : {
//    //    "id_user":"{facebookid}",
//    //    "id_subject":"{subject}",
//    //    "score":"{score}"
//    //				}
//    
//    // Mark: - Question
//    func questionSelectAllByTimeStamp(timeStamp: String) -> String {
//        return encodingStringURL(kApiAddress.Questions.SelectAllByTimeStamp + timeStamp)
//    }
//    
//    
//    func questionSelectByID(id: String) -> String {
//        return encodingStringURL(kApiAddress.Questions.SelectByID + id)
//    }
//    
//    
//    // Mark: - Subjects
//    func subjectsSelectAllByTimeStamp(timeStamp: String) -> String {
//        return encodingStringURL(kApiAddress.Subjects.SelectAllByTimeStamp + timeStamp)
//    }
//    
//    
//    func subjectsSelectByID(id: String) -> String {
//        return encodingStringURL(kApiAddress.Subjects.SelectByID + id)
//    }
//    
//    func subjectsDetail(id: String) -> String {
//        return encodingStringURL(kApiAddress.Subjects.Detail + id)
//    }
//    
//    
//    // Mark: - Users
//    func userLogin(timeStamp: String) -> String {
//        return encodingStringURL(kApiAddress.Users.Login)
//    }
//    
//    
//    
//    private func encodingStringURL(string: String) -> String {
//        return string.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
//    }
//    
//}
