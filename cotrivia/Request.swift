//
//  Request.swift
//  cotrivia
//
//  Created by MacBook Pro on 9/2/16.
//  Copyright © 2016 PT. Code Development Indonesia. All rights reserved.
//

import Foundation


import UIKit
import Alamofire
import SwiftyJSON


// MARK: - RequestProtocol
// RequestProtocol used for base class with implement protocol instead base class

protocol RequestProtocol {}

extension RequestProtocol {
    
    func getRequest(request: RequestParameters, completion: CompletionRequest) {
        Alamofire.request(.GET, request.url, parameters: request.params, encoding: .JSON, headers: request.headers).validate().responseJSON { (response) -> Void in
            
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print(json)
                    completion(isSuccess: true, json: json, error: nil)
                }
            case .Failure:
                completion(isSuccess: false, json: nil, error: response.result.error)
                
            }
            
        }
    }
    
    func postRequest(request: RequestParameters, completion: CompletionRequest) {
        Alamofire.request(.POST, request.url, parameters: request.params, encoding: .JSON, headers: request.headers).validate().responseJSON { (response) -> Void in
            
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print(json)
                    completion(isSuccess: true, json: json, error: nil)
                }
            case .Failure:
                completion(isSuccess: false, json: nil, error: response.result.error)
                
            }
            
        }
    }
    
    func uploadImageRequest(request: RequestParameters, completion: CompletionRequest) {
        let headers = request.headers ?? ["Content-Type": "application/json"]
        
        
        Alamofire.upload(.POST, request.url, headers: headers,
                         multipartFormData: { multipartFormData in
                            
            },
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .Success(let upload, _, _):
                                upload.responseJSON { response in
                                    if let value = response.result.value {
                                        let json = JSON(value)
                                        completion(isSuccess: true, json: json, error: nil)
                                    } else {
                                        completion(isSuccess: false, json: nil, error: nil)
                                    }
                                }
                                
                            case .Failure :
                                completion(isSuccess: false, json: nil, error: nil)
                            }
                            
            }
        )}
    
    
}


// MARK: - RequestParameters
// RequestParameters = struct for passed parameters to Request


struct RequestParameters: RequestProtocol {
    let url: String
    let params : [String: AnyObject]?
    var headers : [String: String]? = nil
    
    init(url: String, params: [String: AnyObject]?, headers: [String: String]? = nil) {
        self.url = url
        self.params = params
        self.headers = headers
    }
}


// MARK: - Request
// Request = struct for passed parameters to Request

class Request: RequestProtocol {
}

