//
//  Subject+CoreDataProperties.swift
//  cotrivia
//
//  Created by MacBook Pro on 9/2/16.
//  Copyright © 2016 PT. Code Development Indonesia. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Subject {

    @NSManaged var id: String
    @NSManaged var name: String
    @NSManaged var iconURL: String
    @NSManaged var lastUpdate: NSDate
    @NSManaged var icon: NSData?
    @NSManaged var questions: NSSet?

}
