//
//  Subject.swift
//  cotrivia
//
//  Created by MacBook Pro on 9/2/16.
//  Copyright © 2016 PT. Code Development Indonesia. All rights reserved.
//

import Foundation
import CoreData


class Subject: NSManagedObject {

    static let coreDataUtil = CoreDataUtil(entityName: "Subject", uniqueID: "id")
    
    class func insertSubjects(data: [AnyObject], context: NSManagedObjectContext, completion: CompletionSuccess?) {
        coreDataUtil.insertObjects(data, context: context, completion: completion)
    }
    
    class func fetchAllSubject(context: NSManagedObjectContext, predicate: NSPredicate? = nil, descriptor: [NSSortDescriptor]? = nil) -> [Subject] {
        return coreDataUtil.fetchAllObjects(context, withPredicate: predicate, sortObjectBy: descriptor) as! [Subject]
    
    }
    
    

}
