//
//  Constants.swift
//  cotrivia
//
//  Created by MacBook Pro on 9/2/16.
//  Copyright © 2016 PT. Code Development Indonesia. All rights reserved.
//

import UIKit
import SwiftyJSON

// Mark: - Global Constant
let kCoreDataContext = (UIApplication.sharedApplication().delegate as! AppDelegate).coreDataStack

// Mark: - TypeAlias
typealias CompletionSuccess = (success: Bool)  -> Void
typealias CompletionListSubjects = (subjects: [Subject])  -> Void
typealias CompletionRequest =  (isSuccess: Bool, json: JSON?, error: NSError?) -> Void

private let kBundleID = "com.codeid.cotrivia"

// Mark: - Constant Enum

enum kApiAddress {


    enum Questions {
        static let SelectAllByTimeStamp = BaseAddress.Questions + "timestamp/"
        static let SelectByID = BaseAddress.Questions + "view/"
      }
    
    enum Subjects {
        static let SelectAllByTimeStamp = BaseAddress.Subjects + "timestamp/"
        static let SelectByID = BaseAddress.Subjects + "view/"
        static let Detail = BaseAddress.Subjects + "detail/"
    }
    
    enum Users {
        static let Login = BaseAddress.Users + "login/"
    }
    
    enum Scores {
        static let SelectByUserID = BaseAddress.Scores + "view"
        static let SaveScore = BaseAddress.Scores
    }
    
    enum BaseAddress {
        static let DefaultAPI = "http://128.199.68.208/cotrivia/api/index.php/"
        static let Questions = BaseAddress.DefaultAPI + "questions/"
        static let Subjects = BaseAddress.DefaultAPI + "subjects/"
        static let Users = BaseAddress.DefaultAPI + "users/"
        static let Scores = BaseAddress.DefaultAPI + "scores/"
    }
}

enum kUserDefault {
    static let BaseUserDefaults = kBundleID + ".userdefaults."
    static let SubjectTimeStamp = BaseUserDefaults + "SubjectTimeStamp"
}

enum kNotification {
    static let BaseNotification = kBundleID + ".notification."
}


enum kSavedKey  {
    
}


enum kPath {
    enum Archived {
        static let User =  NSHomeDirectory().stringByAppendingFormat("/Documents/user.bin")
    }
}


enum kReuseID {
    enum TableViewCell {
       
    }
    
    enum CollectionViewCell {
        static let SubjectCell = kBundleID + ".subjectCell"
    }
}


enum kColor {
    
    enum Gray {
        static let Light = UIColor.groupTableViewBackgroundColor()
        static let Medium = UIColor.lightGrayColor()
        static let Dark = UIColor.darkGrayColor()
    }
    
    static let Green = UIColor.greenColor()
    static let Red = UIColor.redColor()
    static let Clear = UIColor.clearColor()
    static let Black = UIColor.blackColor()
    static let White = UIColor.whiteColor()
}


enum kImageAsset {
    static let Search = UIImage(named: "search")!
    static let ListCategories = UIImage(named: "list")!
}


enum kFontName {
    enum Dosis {
        static let Light = "Dosis-Light"
        static let Regular = "Dosis-Regular"
        static let Medium = "Dosis-Medium"
        static let SemiBold = "Dosis-SemiBold"
    }
}


enum kStoryBoardID {
    static let Home = "HomeStoryboardID"
}


enum kSegueID {
    
}

enum kGlobalQueue {
    static let Main =  dispatch_get_main_queue()
    static let Interactive = dispatch_get_global_queue(Int(QOS_CLASS_USER_INTERACTIVE.rawValue), 0)
    static let UserInitiated = dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.rawValue), 0)
    static let Utility = dispatch_get_global_queue(Int(QOS_CLASS_UTILITY.rawValue), 0)
    static let Background = dispatch_get_global_queue(Int(QOS_CLASS_BACKGROUND.rawValue), 0)
}

