//
//  SubjectsController.swift
//  cotrivia
//
//  Created by MacBook Pro on 9/2/16.
//  Copyright © 2016 PT. Code Development Indonesia. All rights reserved.
//

import Foundation
import MBProgressHUD
import SwiftyJSON
import AFDateHelper

protocol ControllerProtocol {
    func initializeAllObject(hud: MBProgressHUD, completion: CompletionSuccess) -> Void
    func fetchAllObject(completion: (results: [AnyObject])  -> Void)
    func serializeObjectWithJSON (json: JSON) -> [Dictionary<String, AnyObject>]
}

class SubjectC: ControllerProtocol {
    func initializeAllObject(hud: MBProgressHUD, completion: CompletionSuccess) {

        let url = kApiAddress.Subjects.SelectAllByTimeStamp
        let requestParams = RequestParameters(url: url, params: nil)
        
        hud.showAnimated(true)
        dispatch_async(kGlobalQueue.Background) {
            Request().getRequest(requestParams) { (isSuccess, json, error) in
                guard isSuccess else {
                    completion(success: false)
                    hud.hideAnimated(true)
                    return
                }
                
                let SubjectDict = self.serializeObjectWithJSON(json!["Subject"])
                let context = kCoreDataContext.managedObjectContext!
                
                Subject.insertSubjects(SubjectDict, context: context, completion: { (success) in
                    dispatch_async(kGlobalQueue.Main) {
                        UserDefaults().subjectTimeStamp = json!["timestamp"].intValue
                        completion(success: success)
                        hud.hideAnimated(true)
                    }
                })
                
            }
        }

    }
    
    func fetchAllObject(completion: (results: [AnyObject]) -> Void) {
        dispatch_async(kGlobalQueue.Background) {
            let context = kCoreDataContext.managedObjectContext
            let subjects = Subject.fetchAllSubject(context!)
            
            dispatch_async(kGlobalQueue.Main, {
                return subjects
            })
        }
    }
    
    func serializeObjectWithJSON(json: JSON) -> [Dictionary<String, AnyObject>] {
        var subjects : [Dictionary<String, AnyObject>] = []
        for wJson in json.arrayValue {
            
            var subject = [String: AnyObject]()
            subject["title"] = wJson["id"].stringValue
            subject["hits"] = wJson["subject_name"].stringValue
            subject["id"] = wJson["subject_icon"].stringValue
            
            subject["lastUpdate"] = NSDate()
            
            
            subjects.append(subject)
        }
        
        return subjects
    }
}


protocol SubjectController {}


extension SubjectController where Self: ControllerProtocol {
    
    
    func initializeAllSubject(hud: MBProgressHUD, completion: CompletionSuccess){
        
        let url = kApiAddress.Subjects.SelectAllByTimeStamp
        let requestParams = RequestParameters(url: url, params: nil)
        
        hud.showAnimated(true)
        dispatch_async(kGlobalQueue.Background) {
            Request().getRequest(requestParams) { (isSuccess, json, error) in
                guard isSuccess else {
                    completion(success: false)
                    hud.hideAnimated(true)
                    return
                }
                
                let SubjectDict = self.serializeSubjectWithJSON(json!["Subject"])
                let context = kCoreDataContext.managedObjectContext!
                
                Subject.insertSubjects(SubjectDict, context: context, completion: { (success) in
                    dispatch_async(kGlobalQueue.Main) {
                        UserDefaults().subjectTimeStamp = json!["timestamp"].intValue
                        completion(success: success)
                        hud.hideAnimated(true)
                    }
                })
                
            }
        }
    }
    
    
    
    func fetchAllSubject(completion: CompletionListSubjects){
        dispatch_async(kGlobalQueue.Background) {
            let context = kCoreDataContext.managedObjectContext
            let subjects = Subject.fetchAllSubject(context!)
            
            dispatch_async(kGlobalQueue.Main, {
                return subjects
            })
        }
    }
    
    
    
    
    func serializeSubjectWithJSON(json: JSON) -> [Dictionary<String, AnyObject>]  {
        
        var subjects : [Dictionary<String, AnyObject>] = []
        for wJson in json.arrayValue {
            
            var subject = [String: AnyObject]()
            subject["title"] = wJson["id"].stringValue
            subject["hits"] = wJson["subject_name"].stringValue
            subject["id"] = wJson["subject_icon"].stringValue
            
            subject["lastUpdate"] = NSDate()
       
            
            subjects.append(subject)
        }
        
        return subjects
    }
    
    
    
    
    
}