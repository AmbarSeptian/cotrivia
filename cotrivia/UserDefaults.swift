//
//  UserDefaults.swift
//  cotrivia
//
//  Created by MacBook Pro on 9/2/16.
//  Copyright © 2016 PT. Code Development Indonesia. All rights reserved.
//

import Foundation

protocol UserDefaultsProtocol {
    
}

extension UserDefaultsProtocol {
  
    func saveUserDefaults(value: AnyObject?, key: String) {
        NSUserDefaults.standardUserDefaults().setValue(value, forKey: key)
    }
    
    func getUserDefaults(key: String) -> AnyObject? {
        return NSUserDefaults.standardUserDefaults().objectForKey(key)
    }
    
}


class UserDefaults: UserDefaultsProtocol {
    
    var subjectTimeStamp: Int {
        get {
            return self.getUserDefaults(kUserDefault.SubjectTimeStamp) as? Int ?? 0
        }
        
        set {
            self.saveUserDefaults(newValue, key: kUserDefault.SubjectTimeStamp)
        }
    }
    
    
}