//
//  SubjectCollectionViewCell.swift
//  cotrivia
//
//  Created by MacBook Pro on 9/5/16.
//  Copyright © 2016 PT. Code Development Indonesia. All rights reserved.
//

import UIKit
import AlamofireImage
import CoreData

class SubjectCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.imageView.layer.cornerRadius = 10
        self.imageView.clipsToBounds = true
        // Initialization code
    }
    
    override func prepareForReuse() {
        self.imageView = nil
    }
    
    class func configureCell(collectionView: UICollectionView, indexPath: NSIndexPath, subject: Subject?, context: NSManagedObjectContext) -> SubjectCollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(kReuseID.CollectionViewCell.SubjectCell, forIndexPath: indexPath) as! SubjectCollectionViewCell
        
        guard let wSubject = subject else { return cell }
        
        cell.titleLabel.text = wSubject.name
        
        guard wSubject.icon == nil else {
            cell.imageView.image = UIImage(data: wSubject.icon!)
            return cell
        }
        
        cell.downloadImageFromCell(wSubject, context: context)
        
        return cell
    }
    
    
    private func downloadImageFromCell(subject: Subject, context: NSManagedObjectContext) {
        
        self.imageView.af_setImageWithURL(
            NSURL(string: subject.iconURL)!,
            placeholderImage: nil,
            filter: nil,
            imageTransition: .CrossDissolve(0.5),
            runImageTransitionIfCached: true,
            completion: { response in
                
                if response.result.isSuccess {
                    
                    guard let result = response.result.value else {
                       return
                    }
                    
                    subject.icon = UIImageJPEGRepresentation(result, 1.0)
                }
        })
    }

    
    

}
