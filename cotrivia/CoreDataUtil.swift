//
//  CoreDataUtil.swift
//  cotrivia
//
//  Created by MacBook Pro on 9/2/16.
//  Copyright © 2016 PT. Code Development Indonesia. All rights reserved.
//

import Foundation

//
//  CoreDataUtility.swift
//  CloverManga
//
//  Created by MacBook Pro on 8/27/16.
//  Copyright © 2016 MacBook Pro. All rights reserved.
//

import Foundation
import UIKit
import CoreData


protocol CoreDataUtilProtocol {
    var entityName : String { get }
    var uniqueID : String { get }
    
}


extension CoreDataUtilProtocol  {
    
    func insertObjects(data: [AnyObject], context: NSManagedObjectContext, completion: CompletionSuccess?) {
        var object: AnyObject?
        let objects: [AnyObject]?
        let request = NSFetchRequest(entityName: self.entityName)
        
        do {
            objects = try context.executeFetchRequest(request)
            
        } catch let error as NSError {
            print("Error occured \(error.userInfo)")
            objects = nil
        }
        
        
        for (_, value) in data.enumerate() {
            if objects?.count > 0 {
                object = objects?.first
            } else {
                
                object = NSEntityDescription.insertNewObjectForEntityForName(self.entityName, inManagedObjectContext: context)
                let keys: [AnyObject] = value.allKeys
                
                for key in keys {
                    self.addObject(context, object: object, value: value, key: key as! String)
                }
            }
        }
        
        kCoreDataContext.save { (success) in
            if let wCompletion = completion {
                wCompletion(success: success)
            }
        }
        
    }
    
    
    private func addObject(context: NSManagedObjectContext, object: AnyObject?, value: AnyObject, key: String){
//        switch (self.entityName, key) {
//        case ("", "categories") :
//            
//            guard let categories = value.valueForKey(key) as? NSSet else {
//                return
//            }
//            
//            for category in categories.allObjects {
//                
//                let predicate = NSPredicate(format: "name == %@", category as! String)
//                guard let category = Category.fetchCategories(context, predicate: predicate).first else {
//                    continue
//                }
//                
//                category.addMangaObject(object as! Manga)
//            }
//            
//            
//        default:
//            object?.setValue(value.objectForKey(key), forKey: key )
//        }
//        
    }
    
    
    
    
    
    func fetchAllObjects(context: NSManagedObjectContext, withPredicate predicate : NSPredicate? = nil, sortObjectBy sortObject: [NSSortDescriptor]? = nil) -> [AnyObject] {
        
        let request = NSFetchRequest(entityName: self.entityName)
        
        var results = [AnyObject]()
        
        if let wPredicate = predicate {
            request.predicate = wPredicate
        }
        
        
        if let wSortObject = sortObject {
            request.sortDescriptors = wSortObject
        }
        
        do {
            results = try context.executeFetchRequest(request)
            return results
            
        } catch let error as NSError {
            print("Error occured \(error.userInfo)")
            return results
        }
    }
    
    
    func deleteObjects(object: NSManagedObject, context: NSManagedObjectContext) {
        context.deleteObject(object)
        do {
            try context.save()
        } catch let error as NSError {
            print("Error occured \(error.userInfo)")
        }
    }
    
    
    func addSetObject(object: AnyObject?, value: AnyObject, key: String) {
        let setObject = object?.mutableSetValueForKey(key)
        setObject?.addObject(value)
    }
    
}


struct CoreDataUtil: CoreDataUtilProtocol {
    let entityName: String
    let uniqueID: String
    
    init(entityName: String, uniqueID: String) {
        self.entityName = entityName
        self.uniqueID = uniqueID
    }
}


